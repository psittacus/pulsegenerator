Report/report.tex:
	python pulsegenerator.py -m 2 3 -y 2016 -d 0 4

#Report/report.pdf: Report/report.tex
	#pdflatex -output-directory Report report.tex

test: Report/report.pdf

clean:
	rm -fr Report

debug: clean
	python -m pudb pulsegenerator.py -m 2 3 -y 2016 -d 0 4
