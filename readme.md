# moved to [gitlab](https://gitlab.com/liesnikov/pulsegenerator)

script to generate month reports for PE in KPI,not sure that this will fit other universities.

Python 3.5,numpy,matplotlib are required to run script.

No need to set up TeX, one can use overleaf.com (registration required) for compilation.
To generate pdf from .tex without online-services you will need pdflatex with geometry,babel,tabularx,pdfscape,graphicx packages installed.



    usage: pulsegenerator.py [-h] [-n STUDENT STUDENT] [-g GROUP] [-f FACULTY] -m MONTHS MONTHS -y YEAR -d DAYS DAYS [-l LIMS LIMS]

    script to generate reports for PE in KPI,returns .tex file
    
    optional arguments:
    
      -h, --help
  
                        show this help message and exit
            
      -n STUDENT STUDENT, --name STUDENT STUDENT
      
                        student`s name,takes two strings separated by space,if
                        not specified will leave space to write by hand

      -g GROUP, --group GROUP
      
                        student`s group name,if not specified will leave space
                        to write by hand
                        
      -f FACULTY, --faculty FACULTY
  
                        student`s faculty name,if not specified will leave
                        space to write by hand
                        
      -m MONTHS MONTHS, --months MONTHS MONTHS
      
                        months to generate report,from first to second,both
                        inclusive,have to be specified as two integers
                        separated by space
                        
      -y YEAR, --year YEAR
  
			year to generate report,has to be specified as integer
                        
      -d DAYS DAYS, --days DAYS DAYS
  
                        your days of PE,have to be specified as two integers
                        separated by space,where 0 is Monday, 6 - Sunday
                        
      -l LIMS LIMS, --lims LIMS LIMS
  
                        lowerlim and upperlim of your heart rate as two
                        integers separeted by space,morning rate will be in
                        [lwrlim,lwrlim +(uprlim-lwrlim)//3],
                        minute before and after training rates will be in 
                        [lwrlim+(uprlim-lwrlim)//3,lwrlim+(uprlim-lwrlim)//2]
                        and
                        [lwrlim+2*(uprlim-lwrlim)//3,lwrlim+4*(uprlim-lwrlim)//5]
                        respectively.

    look for "Report" dir