import datetime
import calendar
import argparse
import os

from sys import exit

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

UKRMONTHS = {1: 'січень',
             2: 'лютий',
             3: 'березень',
             4: 'квітень',
             5: 'травень',
             6: 'червень',
             7: 'липень',
             8: 'серпень',
             9: 'вересень',
             10: 'жовтень',
             11: 'листопад',
             12: 'грудень'
             }


class Day:
    def __init__(self, year, month, date):
        self.date = datetime.date(year, month, date)
        self.mrngpulse = 0
        self.mntbfrpulse = 0
        self.mntaftrpulse = 0
        self.bfrfeel = 0
        self.aftrfeel = 0
        self.func = 0

    def randomize(self, lwrlim, uprlim, daystatus):
        self.mrngpulse = lwrlim + np.random.randint(0, (uprlim - lwrlim) // 3)
        if daystatus:
            self.mntbfrpulse = lwrlim + np.random.randint((uprlim - lwrlim) // 3,
                                                          (uprlim - lwrlim) // 2)
            self.bfrfeel = np.round(
                2 * (self.mntbfrpulse - self.mrngpulse) / (uprlim - lwrlim)) + 3
            self.mntaftrpulse = lwrlim + np.random.randint(2 * (uprlim - lwrlim) // 3,
                                                           4 * (uprlim - lwrlim) // 5)
            self.aftrfeel = np.round(
                2 * (self.mntaftrpulse - self.mrngpulse) / (uprlim - lwrlim)) + 3
            self.func = lwrlim + \
                np.random.randint(2 * (uprlim - lwrlim) //
                                  3, 2 * (uprlim - lwrlim))

    def as_list(self):
        return [self.date.day, self.mrngpulse, self.mntbfrpulse,
                self.mntaftrpulse, self.bfrfeel, self.aftrfeel, self.func
                ]


class Month:
    def __init__(self, month, year):
        self.days = []
        self.month = month
        self.year = year
        self.numberofdays = calendar.monthrange(self.year, self.month)[1]
        for date in calendar.Calendar().itermonthdays(year, month):
            if date != 0:
                self.days.append(Day(year, month, date))
        self.thatdays = []
        self.monthsum = 0  # sum of all morning pulses
        self.average = 0  # average of all morning pulses
        self.monthdata = {'date': [],  # date itself
                          'morning': [],  # pulse in the morning
                          'minute before': [],  # pulse minute before training
                          'minute after': [],  # pulse minute after training
                          'before feeling': [],  # your feeling, respectively
                          'after feeling': [],
                          'func': [],  # функціональний стан
                          }
        with open("../main-template.tex") as templatefile:
            self.template = templatefile.read()

    def fill_days(self, mydays, lwrlim, uprlim):
        self.thatdays = mydays
        # map(lambda day: day.randomize(lwrlim, uprlim,
        # 1 if day.date.weekday() in self.thatdays
        # else 0
        # ),
        # self.days
        # )
        for day in self.days:
            if day.date.weekday() in self.thatdays:
                day.randomize(lwrlim, uprlim, 1)
            else:
                day.randomize(lwrlim, uprlim, 0)
        self.monthsum = round(sum(i.mrngpulse for i in self.days), 2)
        self.average = self.monthsum / self.numberofdays

    def fill_template(self, student_name, group_name, faculty_name):
        tmp_data = np.array([day.as_list() for day in self.days]).T
        self.monthdata['date'] = tmp_data[0]  # date itself
        self.monthdata['morning'] = tmp_data[1]  # pulse in the morning
        # pulse minute before training
        self.monthdata['minute before'] = tmp_data[2]
        # pulse minute after training
        self.monthdata['minute after'] = tmp_data[3]
        # your feeling respectively
        self.monthdata['before feeling'] = tmp_data[4]
        self.monthdata['after feeling'] = tmp_data[5]
        self.monthdata['func'] = tmp_data[6]  # функціональний стан

        def tmp_func(lst):  # instead of type-casting array to int and then str
            return map(lambda x: str(int(x)), lst)

        self.template = self.template % {'Month': UKRMONTHS[self.month],
                                         'MonthNum': self.month,
                                         'Year': self.year,
                                         'numberofdays': str(self.numberofdays),
                                         'days': '&'.join(tmp_func(self.monthdata['date'])),
                                         'mrngpulseformonth': '&'.join(tmp_func(self.monthdata['morning'])),
                                         'mrngsum': round(self.average, 1),
                                         'mntbfrpulseformonth': '&'.join(tmp_func(self.monthdata['minute before'])),
                                         'mntbfrpercentformonth': '&'.join(tmp_func(self.monthdata['minute before'] * 100 / self.average)),
                                         'mntaftrpulseformonth': '&'.join(tmp_func(self.monthdata['minute after'])),
                                         'mntaftrpercentformonth': '&'.join(tmp_func(self.monthdata['minute after'] * 100 / self.average)),
                                         'bfrfeelformonth': '&'.join(tmp_func(self.monthdata['before feeling'])),
                                         'aftrfeelformonth': '&'.join(tmp_func(self.monthdata['after feeling'])),
                                         'StudentName': student_name,
                                         'GroupName': group_name,
                                         'FacultyName': faculty_name
                                         }

    def plotAndSave(self):
        uprlim = max(max(self.monthdata['morning']),
                     max(self.monthdata['minute before']),
                     max(self.monthdata['minute after'])
                     )
        mpl.rcParams['font.family'] = 'fantasy'
        mpl.rcParams['font.fantasy'] = 'Arial, Ubuntu'

        # plotting ЧСС(уд/хв)
        fig, ax = plt.subplots()
        fig.set_figheight(7)
        fig.set_figwidth(18)
        bar_width = 0.3

        plt.xlim([1, 31])
        plt.ylim([0, uprlim * 1.1])

        plt.bar(self.monthdata['date'], self.monthdata['morning'],
                bar_width, color='k', label='зранку')
        plt.bar(self.monthdata['date'] + bar_width, self.monthdata['minute before'],
                bar_width, color='r', label='на початку')
        plt.bar(self.monthdata['date'] + 2 * bar_width,
                self.monthdata['minute after'], bar_width, color='g', label='в кінці')

        plt.xticks(self.monthdata['date'] + 1 *
                   bar_width, self.monthdata['date'].astype(int))

        plt.xlabel('Дні місяця')
        plt.ylabel('ЧСС(уд/хв)')
        plt.legend(fontsize='small', loc=2, borderaxespad=0., frameon=0)

        plt.savefig(str(self.month) + 'HR' + '.png', dpi=100)
        plt.close()

        # plotting just уд/хв per day
        fig, ax = plt.subplots()
        # using "heuristics" here, god knows what should be here, practically
        # random values
        plt.bar(self.monthdata['date'],
                0.5 * (self.monthdata['func'] + self.monthdata['morning']
                       ) * (self.monthdata['func'] != 0),
                bar_width, color='k')

        fig.set_figheight(7)
        fig.set_figwidth(18)

        bar_width = 0.3
        plt.xlim([1, 31])
        plt.ylim([0, uprlim * 1.2])

        plt.xticks(self.monthdata['date'], self.monthdata['date'].astype(int))
        plt.xlabel('Дні місяця')
        plt.ylabel('уд/хв')

        plt.savefig(str(self.month) + 'FS' + '.png', dpi=100)
        plt.close()

        # plotting relative something-something
        fig, ax = plt.subplots()
        # once again, using "heuristics" here, god knows what should be here,
        # practically random values
        plt.plot(self.monthdata['date'], 50 / (self.monthdata['morning'])
                 * (self.monthdata['func'] - self.monthdata['morning'])
                 * (self.monthdata['func'] != 0), '-', figure=fig)

        fig.set_figheight(7)
        fig.set_figwidth(18)

        plt.xlim([1, 31])
        plt.ylim([0, 65])

        plt.xticks(self.monthdata['date'], self.monthdata['date'].astype(int))
        plt.xlabel('Дні місяця')
        plt.ylabel('%')

        plt.savefig(str(self.month) + 'DR' + '.png', dpi=100)
        plt.close()

    def FirstPage(self, StudentName, GroupName, FacultyName, mydays, lwrlim, uprlim):
        self.fill_days(mydays, lwrlim, uprlim)
        self.fill_template(StudentName, GroupName, FacultyName)
        self.plotAndSave()
        with open("../header-template.tex") as templatefile:
            header_template = templatefile.read()
        return header_template + self.template

    def LastPage(self,
                 StudentName, GroupName, FacultyName,
                 mydays, lwrlim, uprlim
                 ):
        self.fill_days(mydays, lwrlim, uprlim)
        self.fill_template(StudentName, GroupName, FacultyName)
        self.plotAndSave()
        return self.template + '''\n\\end{document}'''

    def RegularPage(self,
                    StudentName, GroupName, FacultyName,
                    mydays, lwrlim, uprlim
                    ):
        self.fill_days(mydays, lwrlim, uprlim)
        self.fill_template(StudentName, GroupName, FacultyName)
        self.plotAndSave()
        return self.template


class Report:
    def __init__(self, firstmonth, lastmonth, year):
        self.allMonths = []
        path = os.getcwd()
        os.mkdir(os.path.join(path, 'Report'))
        os.chdir(os.path.join(path, 'Report'))
        for month in range(firstmonth, lastmonth + 1):
            self.allMonths.append(Month(month, year))

    def saveTex(self, StudentName, GroupName, FacultyName,
                mydays, lwrlim, uprlim
                ):
        with open('report.tex', 'w', encoding='utf-8') as f:
            f.write(self.allMonths[0].FirstPage(StudentName, GroupName,
                                                FacultyName, mydays,
                                                lwrlim, uprlim
                                                )
                    )
            if len(self.allMonths) != 1:
                for month in range(1, len(self.allMonths) - 1):
                    f.write(self.allMonths[month].RegularPage(StudentName,
                                                              GroupName,
                                                              FacultyName,
                                                              mydays,
                                                              lwrlim,
                                                              uprlim
                                                              )
                            )
                f.write(self.allMonths[len(self.allMonths) - 1].LastPage(StudentName,
                                                                         GroupName,
                                                                         FacultyName,
                                                                         mydays,
                                                                         lwrlim,
                                                                         uprlim
                                                                         )
                        )
            else:
                f.write(r'''
                \end{document}''')


parser = argparse.ArgumentParser(description='script to generate reports for PE in KPI, returns .tex file',
                                 epilog="""Hope everything's alright. Look for "Report" dir""")
parser.add_argument('-n', '--name', nargs=2, default=['\_' * 15, ''],
                    help="student's name, takes two strings separated by space, if not specified will leave space to write name by hand"
                    )
parser.add_argument('-g', '--group', default='\_' * 7,
                    help="stufent's group name, if not specified will leave space to write by hand"
                    )
parser.add_argument('-f', '--faculty', default='\_' * 7,
                    help="stufent's faculty name, if not specified will leave space to write by hand"
                    )
parser.add_argument('-m', '--months', nargs=2, required=True, type=int,
                    help='months to generate report, from first to second, both inclusive, have to be specified as two integers separated by space'
                    )
parser.add_argument('-y', '--year', type=int, required=True,
                    help='year to generate report has to be specified as integer'
                    )
parser.add_argument('-d', '--days', nargs=2, type=int, required=True,
                    help='your days of PE, has to be specified as two integers separated by space, where 0 is Monday, 6 - Sunday'
                    )
parser.add_argument('-l', '--lims', nargs=2, type=int, default=[70, 110],
                    help='lowerlim and upperlim of your heart rate as two integers separeted by space, \
morning rate will be in [lwrlim, lwrlim +(uprlim-lwrlim)//3], \
minute before and after training will be in [lwrlim+(uprlim-lwrlim)//3,lwrlim+(uprlim-lwrlim)//2] \
and [lwrlim+2*(uprlim-lwrlim)//3, lwrlim+4*(uprlim-lwrlim)//5] respectively.'
                    )

# parser.print_help()

try:
    args = vars(parser.parse_args())
except BaseException as e:
    print("Sorry, I failed even on parsing.\n")
    exit()

try:
    x = Report(args['months'][0], args['months'][1], args['year'])
    x.saveTex(' '.join(args['name']),
              args['group'], args['faculty'], args['days'],
              args['lims'][0], args['lims'][1])
except FileExistsError as E:
    print("It seems 'Report' dir was left from previous run, please delete it")
    # os.removedir(name='Report')
    exit()
except BaseException as E:
    print(E)
    print("Failed somewhere, real error catching isn't really working for now")
    exit()
